import sys
import time

class Board():
    class History():
        def __init__(self):
            self.history = []
            
        def Submit(self, strIndex, number, candidates=[]):
            if len(candidates) < 2:
                candidates = [number]
            self.history.append((strIndex, number, candidates))
            
        def Reverse(self):
            if len(self.history) <= 1:
                self.history = []
                return 0
            else:
                self.history[-1:] = []
                return len(self.history[-1][2]) # Return the number of choice of tail of remains
        
        def GetTail(self):
            if self.history == []:
                return None
            else:
                return self.history[-1]
        
             
    def __init__(self, cubes=[]):
        self.cubes = []
        self.candidates = []
        self.history = self.History()
        self.starttime = None
        for i in range(81):
            self.cubes.append(' ')
            self.candidates.append([])
        
        for cube in cubes:
            try:
                index = cube[0]
                number = cube[1]
                self.cubes[self.Index(index)] = number
            except:
                print 'Wrong input format: %s' % str(cube)
                sys.exit(-1)
        print 'Solving problem:'
        self.ShowBoard()
        self.RefreshCandidates()
    
    def Index(self, stringIndex):
        if len(stringIndex) < 2:
            raise IndexError('Too short index: %s' % stringIndex)
        if ('0' in stringIndex) or (not stringIndex.isdigit()):
            raise IndexError('Wrong index format: %s' % stringIndex)
        return (int(stringIndex[0])-1)*9 + int(stringIndex[1]) -1
    
    def ReverseIndex(self, numIndex):
        row = numIndex / 9 + 1
        column = numIndex % 9 + 1
        return '%d%d' % (row, column)
      
        row = int(stringIndex[0])
        column = int (stringIndex[1])
        numIndex = (row - 1) * 9 + column - 1
        return numIndex
    
    def ShowBoard(self):
        for row in range(9):
            if (row % 3 == 0):
                print '-' * 22
            print '{} {} {} | {} {} {} | {} {} {}'.format(*self.cubes[(row*9) : (row*9+9)])
        print '-' * 22

    def EmptyCubes(self):
        empties = []
        for i in range(81):
            if self.cubes[i] == ' ':
                empties.append(ReverseIndex(i))
        return empties
    
    def SameRowCubes(self, stringIndex):
        cubes = []
        strRow = stringIndex[0]
        column = int(stringIndex[1])
        for i in range(1,10):
            if column == i:
                continue
            cubes.append(strRow + str(i))
        return cubes
    
    def SameColumnCubes(self, stringIndex):
        cubes = []
        row = int(stringIndex[0])
        strColumn = stringIndex[1]
        for i in range(1,10):
            if row == i:
                continue
            cubes.append(str(i) + strColumn)
        return cubes
    
    def SameSquareCubes(self, stringIndex):
        rows = [4, 5, 6]
        columns = [4, 5, 6]
        row = int(stringIndex[0])
        column = int(stringIndex[1])
        if row < 4:
            rows = [1, 2, 3]
        elif row > 6:
            rows = [7, 8, 9]
        if column < 4:
            columns = [1, 2, 3]
        elif column > 6:
            columns = [7, 8, 9]
        cubes = [(str(x) + str(y)) for x in rows for y in columns]
        cubes.remove(stringIndex)
        return cubes
    
    
    def AvailableNumbers(self, stringIndex):
        numbers = [1,2,3,4,5,6,7,8,9]
        for strIndex in set(self.SameRowCubes(stringIndex) + self.SameColumnCubes(stringIndex) +
                            self.SameSquareCubes(stringIndex)):
            if self.cubes[self.Index(strIndex)] in numbers:
                numbers.remove(self.cubes[self.Index(strIndex)])
        return numbers
        
    def RefreshCandidates(self):
        for i in range(81):
            if self.cubes[i] != ' ':
                self.candidates[i] = []
            else:
                candidates = self.AvailableNumbers(self.ReverseIndex(i))
                if candidates == []:
                    return False
                self.candidates[i] = candidates
        return True
    
    
    def DeterministicFill(self):
        if not self.RefreshCandidates():
            return False
        hit = False
        valid = False
        for i in range(81):
            if len(self.candidates[i]) == 1:
                number = self.candidates[i][0]
                self.cubes[i] = number
                self.history.Submit(self.ReverseIndex(i), number)
                hit = True
                break
        if hit:
            valid = self.RefreshCandidates()
            if (not valid) or (valid and not self.DeterministicFill()):
                strIndex, number, candidate  = self.history.GetTail()
                self.cubes[self.Index(strIndex)] = ' '
                self.history.Reverse()
                self.RefreshCandidates()
                return False
        return True
        
    def IsComplete(self):
        for i in range(81):
            if self.cubes[i] == ' ':
                return False
        return True
    
    def LeastCandidateCube(self):
        bestLength = 10
        bestIndex = -1
        for i in range(81):
            if len(self.candidates[i]) < 2:
                continue
            if len(self.candidates[i]) < bestLength:
                bestIndex = i
                bestLength = len(self.candidates[i])
        if bestIndex == -1:
            raise SolveError('Empty cube exists while no available candidate.')
        return self.ReverseIndex(bestIndex)
        
    def FillBoard(self):
        if self.starttime is None:
            self.starttime = time.time()
        if not self.DeterministicFill():
            return False
        # Check whether the board is fully filled first, if Yes, done, output
        if self.IsComplete():
            print "Done. Time elapsed: %d seconds" % (time.time()-self.starttime)
            self.ShowBoard()
            return True
        # choose the cube with shortest length of candidates > 1
        strIndex = self.LeastCandidateCube()
        index = self.Index(strIndex)
        for number in self.candidates[index]:
            # Fill and submit to the history
            self.cubes[index] = number
            self.history.Submit(strIndex, number, self.candidates[index])
            # Refresh the board
            valid = self.RefreshCandidates()
            # if False, or True but Call FillBoard Recursively returns False
            if not (valid and self.FillBoard()):
                # Reverse history
                self.cubes[index] = ' '
                self.history.Reverse()
                self.RefreshCandidates()
                continue
            else:
                return True
        # if all candidates fails
        # Perform deep history reverse
        while (len(self.history.GetTail()[2]) == 1):
            preStrIndex = self.history.GetTail()[0]
            self.cubes[self.Index(preStrIndex)] = ' '
            self.history.Reverse()
        # return False
        self.RefreshCandidates()
        return False
    
    
if __name__ == '__main__':
    board = Board([('11',8), ('32',7), ('42',5),('92',9),
                   ('23',3), ('73',1), ('83',8),
                   ('24',6), ('64',1), ('84',5),
                   ('35',9), ('55',4), ('46',7), ('56',5),
                   ('37',2), ('57',7), ('97',4),
                   ('68',3), ('78',6), ('88',1), ('79',8)])
    board.FillBoard()
    raw_input()
