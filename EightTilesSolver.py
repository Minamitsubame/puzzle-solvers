__author__ = 'Jack x'
import sys
from copy import deepcopy
from collections import deque

possibilities = {
    (0, 0): ((1, 0), (0, 1)),
    (0, 1): ((0, 0), (0, 2), (1, 1)),
    (0, 2): ((0, 1), (1, 2)),
    (1, 0): ((0, 0), (2, 0), (1, 1)),
    (1, 1): ((0, 1), (2, 1), (1, 0), (1, 2)),
    (1, 2): ((0, 2), (2, 2), (1, 1)),
    (2, 0): ((1, 0), (2, 1)),
    (2, 1): ((1, 1), (2, 0), (2, 2)),
    (2, 2): ((1, 2), (2, 1))
}

numeric_order = (
    (0, 0), (0, 1), (0, 2),
    (1, 0), (1, 1), (1, 2),
    (2, 0), (2, 1), (2, 2)
)


class Board:
    def __init__(self, board_in):
        if len(board_in) != 9:
            print 'Wrong input size of Board'
            sys.exit(-1)
        # board_in is a dictionary with keys from 1 to 9
        self.board = board_in
        self.reverse = {pos: num for num, pos in self.board.iteritems()}

    def possible_moves(self):
        return possibilities[self.board[9]]

    def get_numeric_rep(self, swap=((0, 0), (0, 0))):
        modified = self.reverse
        if swap != ((0, 0), (0, 0)):
            modified = deepcopy(self.reverse)
            modified[swap[0]], modified[swap[1]] = self.reverse[swap[1]], self.reverse[swap[0]]
        result = 0
        for pos in numeric_order:
            result = result * 10 + modified[pos]
        return result

    def get_board_after_move(self, swap=(0, 0)):
        modified = self.reverse
        if swap != ((0, 0), (0, 0)):
            modified = deepcopy(self.reverse)
            modified[swap[0]], modified[swap[1]] = self.reverse[swap[1]], self.reverse[swap[0]]
        return Board({num: pos for pos, num in modified.iteritems()})


def recover_path(history_in):
    mutable = [(history_in[123456789])]
    path = []
    while (mutable[0][0]) is not None:
        path.append((mutable[0])[1])
        mutable[0] = history_in[(mutable[0][0]).get_numeric_rep()]
    path.reverse()
    print path


# def is_reverse(a, b):
#    return (a[0] == b[1]) and (a[1] == b[0])


if __name__ == '__main__':
    current_dic = {
        1: (1, 2), 2: (0, 2), 3: (0, 1),
        4: (1, 0), 5: (1, 1), 6: (0, 0),
        7: (2, 0), 8: (2, 1), 9: (2, 2)
    }
    current = Board(current_dic)
    queue = deque()
    history = {current.get_numeric_rep(): (None, None)}
    for p in current.possible_moves():
        queue.append((current, (current.board[9], p)))

    while len(queue) > 0:
        board, move = queue.popleft()
        afterMove = board.get_board_after_move(move)
        afterMoveNumeric = afterMove.get_numeric_rep()

        if afterMoveNumeric in history.keys():
            continue
        elif afterMoveNumeric == 123456789:
            history[123456789] = (board, move)
            recover_path(history)
            sys.exit(0)
        else:
            history[afterMoveNumeric] = (board, move)
            for p in afterMove.possible_moves():
                queue.append((afterMove, (afterMove.board[9], p)))