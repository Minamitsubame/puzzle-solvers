import sys

target = [1,2,3,4,5,6,7,8,9]
working = []
history = []
boardHistory = []
init = [3,7,5,6,8,4,9,2,1]

def LeftUpper(board, reverse=False):
    output = board[:]
    if not reverse:
        output[0] = board[3]
        output[1] = board[0]
        output[3] = board[4]
        output[4] = board[1]
    else:
        output[3] = board[0]
        output[0] = board[1]
        output[4] = board[3]
        output[1] = board[4]
    return output

def LeftLower(board, reverse=False):
    output = board[:]
    if not reverse:
        output[3] = board[6]
        output[4] = board[3]
        output[6] = board[7]
        output[7] = board[4]
    else:
        output[6] = board[3]
        output[3] = board[4]
        output[7] = board[6]
        output[4] = board[7]
    return output

def RightUpper(board, reverse=False):
    output = board[:]
    if not reverse:
        output[1] = board[4]
        output[2] = board[1]
        output[4] = board[5]
        output[5] = board[2]
    else:
        output[4] = board[1]
        output[1] = board[2]
        output[5] = board[4]
        output[2] = board[5]
    return output

def RightLower(board, reverse=False):
    output = board[:]
    if not reverse:
        output[4] = board[7]
        output[5] = board[4]
        output[7] = board[8]
        output[8] = board[5]
    else:
        output[7] = board[4]
        output[4] = board[5]
        output[8] = board[7]
        output[5] = board[8]
    return output

def ReverseInterface(board, step):
    if step == 'lu':
        return LeftUpper(board, True)
    elif step == 'll':
        return LeftLower(board, True)
    elif step == 'ru':
        return RightUpper(board, True)
    else:
        return RightLower(board, True)


def BoardLookUp(board):
    global history
    for (his, preStep) in history:
        if his == board:
            return preStep
    


def RecoverStep(lastStep):
    global history, init
    steps = [lastStep]
    preBoard = ReverseInterface(target, lastStep)
    preStep = ''
    while (preBoard != init):
        preStep = BoardLookUp(preBoard)
        steps[0:0] = [preStep]
        preBoard = ReverseInterface(preBoard, preStep)
    print steps


working.append(init)
history.append((init, '.'))
boardHistory.append(init)

def RecursiveSolver():
    global working, history, target
    preBoardCnt = len(working)
    for board in working:
        lu = LeftUpper(board)
        if lu == target:
            RecoverStep('lu')
            return
        elif lu not in boardHistory:
            history.append((lu, 'lu'))
            boardHistory.append(lu)
            working.append(lu)
        
        ll = LeftLower(board)
        if ll == target:
            RecoverStep('ll')
            return
        elif ll not in boardHistory:
            history.append((ll, 'll'))
            boardHistory.append(ll)
            working.append(ll)
            
        ru = RightUpper(board)
        if ru == target:
            RecoverStep('ru')
            return
        elif ru not in boardHistory:
            history.append((ru, 'ru'))
            boardHistory.append(ru)
            working.append(ru)
            
        rl = RightLower(board)
        if rl == target:
            RecoverStep('rl')
            return
        elif rl not in boardHistory:
            history.append((rl, 'rl'))
            boardHistory.append(rl)
            working.append(rl)
    working[:preBoardCnt] = []
    RecursiveSolver()
    
RecursiveSolver()
    





