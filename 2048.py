__author__ = 'Jack x'

# 2048 Solver


class Board:
    def __init__(self):
        self.numbers = {
            (1, 1): None, (1, 2): None, (1, 3): None, (1, 4): None,
            (2, 1): None, (2, 2): None, (2, 3): None, (2, 4): None,
            (3, 1): None, (3, 2): None, (3, 3): None, (3, 4): None,
            (4, 1): None, (4, 2): None, (4, 3): None, (4, 4): None
        }
        self.expected_space = 16

    def left(self):
        pass

    def right(self):
        pass

    def up(self):
        pass

    def down(self):
        pass

    def fill_in(self, pos, number):
        self.numbers[pos] = number


if __name__ == "__main__":
    pass
